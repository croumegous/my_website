class Body{
  constructor(){
    this.x = innerWidth/2;
    this.y = innerHeight/1.26;
    this.vy = 0;
    this.vx = 0;
    this.v = 4;
    this.win = false;
  }
  
  show(){
    fill(51);
    circle(this.x, this.y, innerWidth*innerHeight/108000);
  }
  
  left(){
    this.vx += -this.v;
  }
  
  right(){
    this.vx += this.v;
  }
  
  down(){
    this.vy += this.v;
  }
  
  up(){
    this.vy += -this.v;
  }
  
  stop(){
    this.vx = 0;
    this.vy = 0;
  }
  
  move(){
    this.y += this.vy;
    this.x += this.vx;
    
    if(this.checkHitbox()){
      this.fail();
    }
    if(collideRectCircle(innerWidth/2.1, innerHeight/15, innerWidth/10, innerHeight/4,this.x,this.y,8)){
      this.win = true;
    }
  }
  
  checkHitbox(){
    
    if(collideRectCircle(innerWidth/4, innerHeight/1.49, innerWidth/2.31, innerHeight/18,this.x,this.y,8)){
      return true;
    }
    
    if(collideRectCircle(innerWidth/2.8, innerHeight/1.81, innerWidth/2.5, innerHeight/15.4,this.x,this.y,8)){
      return true;
    }
    
    if(collideRectCircle(innerWidth/3.67, innerHeight/2.445, innerWidth/2.45, innerHeight/9.5,this.x,this.y,8)){
     return true; 
    }
    
    if(collideRectCircle(innerWidth/1.395, innerHeight/1.64, innerWidth/10, innerHeight/8.5,this.x,this.y,8)){
     return true; 
    }
    
    if(collideRectCircle(innerWidth/2.16, innerHeight/3.15, innerWidth/20, innerHeight/10,this.x,this.y,8)){
     return true; 
    }
    
    
    if(collideRectCircle(innerWidth/1.88, innerHeight/3.15, innerWidth/4, innerHeight/16,this.x,this.y,8)){
     return true; 
    }
    
    
    
    if(collideLineCircle(innerWidth/3.9, innerHeight, innerWidth/3.9, innerHeight/1.5,this.x,this.y,8)){
      return true;
    }
    
    if(collideLineCircle(innerWidth/1.37, innerHeight, innerWidth/1.37, innerHeight/1.5,this.x,this.y,8)){
      return true;
    }
    
    if(collideLineCircle(innerWidth/4, innerHeight/1.16, innerWidth/1.3, innerHeight/1.16,this.x,this.y,8)){
      return true;
    }
    
    if(collideLineCircle(innerWidth/2.97, innerHeight/1.5, innerWidth/2.97, innerHeight/3,this.x,this.y,8)){
      return true;
    }
    
     if(collideLineCircle(innerWidth/1.435, innerHeight/1.7, innerWidth/1.435, innerHeight/3,this.x,this.y,8)){
      return true;
    }
    
    return false;
  }

  fail(){
    this.x = innerWidth/2;
    this.y = innerHeight/1.26;
    this.vx =0;
    this.vy;
  }

}
  
  