let body;
let lines = [];
let rects = [];

function preload(){
  imgParcour = loadImage('./img/parcour_souriciere.jpg');
}


function setup() {
  
  var canvas = createCanvas(innerWidth , innerHeight);
  image(imgParcour, 0, 0);
  body = new Body();
    
}

function keyPressed(){
  if(keyCode === LEFT_ARROW){
    body.left(); 
  }
  if(keyCode === RIGHT_ARROW){
    body.right();    
  }
  if(keyCode === DOWN_ARROW){
    body.down();    
  }
  if(keyCode === UP_ARROW){
    body.up();    
  }  
  
}

function keyReleased(){
  body.stop();
}



function draw() {
  background(imgParcour);
  
  body.show();
  body.move();
  /*
  lines.push(line(innerWidth/3.9, innerHeight, innerWidth/3.9, innerHeight/1.5));
  lines.push(line(innerWidth/1.37, innerHeight, innerWidth/1.37, innerHeight/1.5));
  lines.push(line(innerWidth/4, innerHeight/1.16, innerWidth/1.3, innerHeight/1.16));
  
  lines.push(line(innerWidth/2.97, innerHeight/1.5, innerWidth/2.97, innerHeight/3));
  
  lines.push(line(innerWidth/1.435, innerHeight/1.7, innerWidth/1.435, innerHeight/3));
  
  
  
  rects.push(rect(innerWidth/4, innerHeight/1.49, innerWidth/2.31, innerHeight/18));
  
  rects.push(rect(innerWidth/2.8, innerHeight/1.81, innerWidth/2.5, innerHeight/15.4));
  
  rects.push(rect(innerWidth/3.67, innerHeight/2.445, innerWidth/2.45, innerHeight/9.5));
  
  rects.push(rect(innerWidth/1.395, innerHeight/1.64, innerWidth/10, innerHeight/8.5));
  
  rects.push(rect(innerWidth/2.16, innerHeight/3.15, innerWidth/20, innerHeight/10));
  
  rects.push(rect(innerWidth/1.88, innerHeight/3.15, innerWidth/4, innerHeight/16));
  
  
  rects.push(rect(innerWidth/2.1, innerHeight/15, innerWidth/10, innerHeight/4));
  
  */
  
  
  if(body.win){
    //image(gif_loadImg, 50, 50);
  
  //gif_createImg.position(50, 350);
    
    alert('You win');
    
    //gifRickroll = createImg('./img/rickroll.gif', 'Never gonna give you up');
    //gifRickroll.position(innerWidth/2, innerHeight/2);
    body.win = false;
    body.fail();
  }
}





