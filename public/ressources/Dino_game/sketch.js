let body;
let crabs = [];
let cImg;
let bImg;
let dImg;
let scoreElem ;
let score = 0;
let loop = true;

let hitboxActive = false ;

function preload(){
  cImg = loadImage('./img_dino_game/crab2.png');
  bImg = loadImage('./img_dino_game/background3.jpg');
  dImg = loadImage('./img_dino_game/dinosaure.png');
}


function setup() {  
  
  var canvas = createCanvas(innerWidth -20 , innerHeight -20);
  
  
  body = new Body();
  
  showTopBar();
  
  scoreElem = createP('Score = ');
  scoreElem.position(innerWidth * 0.85, 35);  
  scoreElem.style('color', 'white');
  
  setInterval(timer, 100);

}

function timer(){
  if (this.loop){
  score ++;

  scoreElem.html('Score = ' + score);
  }
}


function keyPressed(){
  if(key == ' '){
    body.jump();
  }
  
}
  
function draw() {

  

  if (random(1) < 0.007){
    crabs.push(new Crab());
  }

  
  
  

  background(bImg);   
  body.show();
  body.move();


  for(let c of crabs){
    c.show();
    c.move();

    if(body.hits(c)){
     noLoop();
     this.loop = false;

    }
  }
 

  
}

function showTopBar(){
  
  

    let checkbox;

    checkbox = createCheckbox('Show hitbox', false);
    checkbox.position(20, 25);
    checkbox.changed(myCheckedEvent);


    var col = color(0, 140, 186);
    var col = color(250,0,0);
    let button;
    button = createButton('Try again');
    button.position(165, 25);
    button.style('background-color', col);
    button.style('color', color(255, 255, 255));
    button.size(150, 50)
    button.mousePressed(clicButton);
}

function myCheckedEvent() {
  if (this.checked()){
    hitboxActive = true;
    console.log('checked');
    canvas.focus();
  }
  else {
    hitboxActive = false;
    console.log('unchecked');
    canvas.focus();
  }
}

function clicButton(){
  location.reload();

}