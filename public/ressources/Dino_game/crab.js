class Crab {
  
  constructor(){
    this.offset = innerHeight /6;
    this.h = 100;
    this.size = 90;
    this.x = innerWidth;
    this.y = innerHeight - this.h - this.offset;    
  }
  
  move(){
    this.x -= 10;
  }
  
  show(){
    
    image(cImg, this.x, this.y, this.size + this.size /2 - 10, this.size -10);   
    if(hitboxActive === true){
        fill(255, 50);
        rect(this.x +15 , this.y +25, this.size + this.size /2 -40, this.size-40);
    }
  }
    
    
  
  
}