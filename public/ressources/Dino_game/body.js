class Body{
  constructor(){
    this.offset = innerHeight /6;
    this.size = 150;
    this.x = 50;
    this.y = innerHeight - this.size - this.offset;
    this.vy = 0;
    this.gravity = 1;
    this.score = 0;
  }
  

  
  
  jump(){
    if(this.y == height - this.size - this.offset){
      this.vy = -20;
    }
  }
  
  move(){
    this.y += this.vy;
    this.vy += this.gravity;
    this.y = constrain(this.y, 0, height  - this.size - this.offset);
  }
  
  show(){

    
    

   image(dImg, this.x, this.y, this.size, this.size); 
   if(hitboxActive === true){
      fill(255, 50);
      ellipse(this.x + this.size /2, this.y + this.size /2 - 10, this.size - 10);      
   }
  
  }
  

  hits(c){
    return collideRectCircle(c.x + 20 , c.y +30 , c.size + c.size/2 -50, c.size -50, this.x + this.size /2, this.y + this.size /2 - 15, this.size - 15);
  }

}